export const osPoem = {
    name: "O.S.",
    image: "/parmanPoemImages/os.jpg",
    tags: ['Fear', 'Fight'],
    body: `
    Sapiens killed the Neanderthals.
    Eve ate an Apple.
    A people were brough to a country in chains.
    
    The economic original sin of a country is that its currency cannot be used to invest in itself in the long term.

    What’ll happen to America?
    Original sins aren’t forgotten.
    
    Redemption is coming.
    
    (I Can’t Breathe)    
    `
}