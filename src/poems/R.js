export const RPoem = {
    name: "R.",
    image: "/parmanPoemImages/R.jpg",
    tags: ["Searching", "Human Collection"],
    body: `I love that you are ambitious.
    Always striving to become better.
    Anything less than your best? Complacency!

    You run a marathon when the requirement is a 5K.
    An Achilles who's heel begs to be hit.
    Outpace, Outlast, Outshine.

    I love that you want to be unique.
    A black swan.
    One in a billion.
    Alone.

    I hope that one day you can belong.
    So that when you grow old,
    The bags under your eyes carve out smile wrinkles on your cheeks.
    `,
}