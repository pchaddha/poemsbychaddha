export const RebuildPoem ={
    name: "Rebuild",
    image: "/parmanPoemImages/",
    tags: ['Hope', 'Potential'],
    body: `The first tick of afternoon sniped his face with a beam of sunlight,
    Through the high fence of the house,
    Through the little crevice of the patio, 
    Through the translucent window of his rental basement room.
    It jolted him up.
    
    He rubbed his face, with dry hands, and immediately felt the headache of the liquor consumed 
	In his shame, disappointment, and self-loathing. 
    
    Why-
	Why had his best friend deserted him?
	Why had his girlfriend left him?
	Why had he,
    Who always thought of himself as ‘King’
    Been succumbed to the acts of a beggar?
    There had to be something wrong with him.
    
    He would not allow himself,
    despite the situation, 
    and his destructive habits,
    To claim “victim” status. 
    
    He was not a victim. 
    He would not allow himself to be the beggar.
    He was the King. 
    
    He threw his legs of the bed, with the intent of action, 
	And gazed upon the poster by the side of his desk 
    He stared at it intently.
    It depicted a man surrounded by dragons in 3 directions, and by darkness in the last. 
    He sat in the middle, in a small forge protected from hellfire. 
	A Semblance of the Grace of God. 
    The forge seemed crippled with rust.
	Incapable. 
    No ingots.
    The dragons extended outwardly and endlessly until almost the very top.
	Radial extension from the forge was proportional to the size of dragon. 
    The top edge of the poster contained a Behemoth, a Leviathan, or a Hydra.
	But behind this monster, 
	Appeared to be a sky blue in color, and free from the smog of the current environment. 
    The bottom edge contained a woman, waiting to embrace the man.
	Waiting to welcome him into nothingness. 
    
    
    
    That act was his inspiration. 
	The act of falling down into Chaos,
	And forging your own Iron from the fires of Hell,
	And coming back to shatter the glass ceiling,
	Looking better than ever. 
    The act of Redesigning, Rebuilding, Reclaiming.
    
    That was the only Code he knew.
	The Code of hard work, perseverance, and ability.
	He was in Chaos.
	It was time to get up.
    Time to fight through Hell.
    Time to improve.
    Time to forge his broken Will.
	Time to reclaim his kingdom. 
    A code to expand a kingdom into an empire. 
    `
};