export const thePerfectRelationshipPoem = {
    name: "The Perfect Relationship" ,
    image: "/parmanPoemImages/thePerfectRelationship.png",
    tags: ['Love', 'Relationships'],
    body: `
    Soul mates aren't formed.
    They are built.
    `,
}