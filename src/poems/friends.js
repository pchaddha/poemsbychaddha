export const FriendsPoem = {
    "name": "Friends",
    "image": "/parmanPoemImages/theFriends.jpg",
    "body": `
        I love hearing them laugh.
        It's a symphony, unpracticed but perfect.
        It might be a game or a concoction.
        But it has the same effect.

        A Wrinkle in Time.
    `,
    "tags": ["Friends", "Relationships"]
};