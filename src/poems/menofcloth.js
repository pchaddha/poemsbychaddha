//List All Poem Titles Here. 
const menOfClothTitle = "The Men of the Cloth";

const menOfClothImage = "/parmanPoemImages/menofcloth.jpg"

//PoemBody
const menOfClothBody = `We are resolute.
Full of pride.
Full of control.
Full of wisdom.

With hands behind our back,
And our chest puffed out,
We are the last- the last men.

We are the men of the cloth.
The men whose actions moved the heavens.
The men whose actions moved hell.
The men whose actions defined 'man'.

But Time,
That lass, whose ticking defines legacy-
She promises the fruit of wisdom, whilst taking away the last of our strength.

And so it is,
These men of the cloth,
Made weak by age,
But strengthened in mind,
Realize
That love commands life.

And

Even these men,
Who moved the Earth,
Are helpless when their sons leave.

And even they cry when mortality reminds them that they are not in control.`;

export const menOfClothPoem = {
    name: menOfClothTitle,
    image: menOfClothImage,
    body: menOfClothBody,
    tags: ['Time', 'Change'],
}