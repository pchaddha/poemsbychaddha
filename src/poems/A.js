export const APoem = {
    name: 'A.',
    image: "/parmanPoemImages/a.jpg",
    tags: ['Human Collection'],
    body: `
    When I look at her, all I see are her eyes.
    Their intensity.
    She behaves like a squirrel, distracted and unsure.
    Going about her purpose.

    She doesn’t know.
    That she can control any room.
    Level anyone with her eyes.
    Stare into a man’s soul, rip out his heart, and leave while blowing a kiss.

    She hates her eyes.
    Her curiosity, knowledge, and command are a burden.
    She wants to love.
    She can’t wait.

    She goes to the doctor with the magic hands.
    He changes her facial chemistry.
    He makes her eyes smaller.
    His lips have the same curvature as an undertaker.

    Now when I look at her, I see her face.
    She blends in with the other faces.
    Her eyes no longer command the room.

    But, I think she is happy.
    I know she is in love.
    I do not blame her for what she did.
    Glorious purpose is a burden.
    But I miss her eyes.
    `,
}