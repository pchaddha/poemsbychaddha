export const theWindowPoem = {
    name: "The Window",
    image: "/parmanPoemImages/window.jpg",
    tags: ['Hope'],
    body: `
    He lived in a basement.
        He slept on a mattress.
            Filled with air;
                A balloon.

    Sleep, a balancing act.
        Always tense and back pain.
            No rest for the fallen.

    He wanted to give up.
        Not his life but his life.
            Move, Bike, Travel, Love.

    Looking out the window.
        A small 6 x 18.
            Dreaming a better life.

    His hope told him to keep
        Going. With a promise
            To unburden the soul.
                To experience joy.
                    A better tomorrow.
                        Relative to today.

    `,
};