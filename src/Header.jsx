import React, {useState, useEffect} from "react";
import {Grid, Typography, Avatar} from "@material-ui/core";
import {minScreenWidths} from "./ConfigurationParameters";

const titleStyle = {
    textAlign:'center', 
    color:'lightblue',
    textShadow: '1px 1px 1px black',
}

const defaultStyleParameters = {
    pictureWidth: '5.0vw',
    pictureHeight: '5vw',
    titleSpace: 12,
    imageSpace: 1,
    titleVariant: 'h1'
}

const smallScreenParameters = {
    pictureWidth: '40vw',
    pictureHeight: '20vw',
    titleSpace: 12,
    imageSpace: 6,
    titleVariant: 'h3'
}

export default function Header ({height, width}) {

    const [styleParameters, setStyleParameters] = useState(defaultStyleParameters)

    useEffect ( () => {
        if (width < minScreenWidths['medium']) {
            setStyleParameters(smallScreenParameters);
        } else {
            setStyleParameters(defaultStyleParameters);
        }
    }, [height, width])    

    return (
        <>
        <Grid container style={{paddingTop:'10px', backgroundImage: "URL(./daisy.jpg)"}}>

            <Grid item xs={styleParameters['titleSpace']} alignItems= 'center' justify='center' alignContent = 'center'> 
                <Typography style={titleStyle} variant={styleParameters.titleVariant} component="h1">
                        Poems By Chaddha 
                </Typography>
            </Grid>
        
        </Grid>
        </>
    )
}