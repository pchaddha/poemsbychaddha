import React, { useState, useEffect} from "react";
import {useLocation, useHistory} from 'react-router-dom'
import {Grid, Button, Typography} from "@material-ui/core";
import {AppBar, Tabs, Tab, Box} from "@material-ui/core";
import {minScreenWidths} from "./ConfigurationParameters"
import {Writer} from "./mainPage/Writer";
import CallIcon from '@material-ui/icons/Call';
import {Icon} from '@material-ui/core';
import { Receipt } from "@material-ui/icons";

const ParmanIcon = () => {
    return (
    <Icon>
        <img style={{width:25}} src="./portriat_floor.svg"/>
    </Icon>
    )
}

const KhushiIcon = () => {
    return (
    <Icon>
        <img style={{width:25}} src="./khushleen_floor.svg"/>
    </Icon>
    )
}

const defaultMainNavbarStyles = {
    direction:'row',
    buttonMarginTop: '10vh',
    buttonPadding: '20px'
}
const smallMainNavbarStyles = {
    direction:'column',
    buttonMarginTop: '-2.5vh',
    buttonPadding: '1vh',
}

const writers = [
    {'name': 'Parmandeep', 'icon': <ParmanIcon style={{width:'1000px'}} />, 'image': "./portriat_floor.jpg", 'route':'/parman_poems'},
    {'name': 'Khushleen', 'icon': <KhushiIcon />, 'image': "./khushleen_floor.jpg", 'route':'/khushi_poems'},
]

/**
 * @function MainNavbar: The navbar used on the home-page (styled differently)
 */
export function MainNavbar ({height, width}) {

    const [styleParameters, setStyleParameters] = useState(defaultMainNavbarStyles);

    let history = useHistory();

    useEffect ( () => {
        if (width < minScreenWidths['medium']) {
            setStyleParameters(smallMainNavbarStyles);
        } else {
            setStyleParameters(defaultMainNavbarStyles);
        };
    }, [height, width]);    

    let Badges = writers.map( (writer, index) => {
        return <Writer height={height} width={width} {...writer} />
    })

    return (
        <>
            <Grid container direction={styleParameters['direction']}>
                {Badges}
            </Grid>

            <Button style={{marginLeft: '5vw',
                            marginTop: styleParameters['buttonMarginTop'],
                            padding: styleParameters['buttonPadding'], borderRadius: '20px', width: '90vw'}}
                    variant='contained' 
                    color='primary'
                    onClick = { () => history.push('/contact')}
                    > 

                <Typography variant='h5'> Contact US </Typography>

            </Button>
        </>
    )
}

function LinkTab({label, iconToUse, index, linkToPush, handleChange}) {
  let history = useHistory();

  return (
    <Tab
      component="a"
      onClick={(event) => history.push(linkToPush)}
      label={label}
      icon={iconToUse}
    />
  );
}

export function Navbar({pathIndex, height, width}) {

  let contactUs = {'name': "Contact Us", 'icon': <CallIcon/>, route:'/contact'};

  let variant = width > minScreenWidths['medium'] ? 'standard' : 'fullWidth';
  
  return (
    <div>
      <AppBar position="static">
        <Tabs
          value={pathIndex}
          centered = {true}
          variant = {variant}
          style={{backgroundColor: 'whitesmoke', color:'black'}}
          aria-label="nav tabs example"
        >
            {[...writers, contactUs].map((row,index) => 
                <LinkTab 
                    value={index}
                    key={row.name}
                    label={row.name}
                    iconToUse={row.icon}
                    linkToPush={row.route}
                    index={index} />
            )}
        </Tabs>
      </AppBar>
    </div> 
  );
}

// const locationDidNotChange = (prevProps, newProps) => {
//   console.log(prevProps);
//   console.log(newProps);
//   return (
//     false
//   )
// }

// export const MemoNavbar = React.Memo(Navbar, locationDidNotChange);