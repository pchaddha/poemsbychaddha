import React from "react";
import {Typography, Button} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import "../notFoundPage/NotFoundPage.css";

export default function NotFoundPage ({window}) {

    let history = useHistory()

    return (
        <div class='notFoundPage'>

            <Typography > 

                What are you looking for ? <br/>

                What am I looking for ? <br/>

                I do not know. <br/>

                All I know is...<br/> 

                This page that you requested... <br/>

                Well, it doesn't exist. <br/>

            </Typography>

            <Typography variant='h1' component="h1">

                Sorry darling. It's a 404. 

            </Typography>

            <Button variant='outlined' color = 'secondary' onClick={()=>history.push("/")}> 
                Let me take you back to the home page ?
            </Button>
        </div>
    )
}