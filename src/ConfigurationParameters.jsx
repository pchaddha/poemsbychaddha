/** Configuration Parameters for the Project */

export const minScreenWidths = {
    'x-small': 100,
    'small': 500,
    'medium': 1000, 
    'large': 1500,
    'x-large': 2000
}
