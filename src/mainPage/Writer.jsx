import React, {useState, useEffect} from "react";
import {minScreenWidths} from "../ConfigurationParameters";
import {Avatar, Card, CardActionArea, CardActions, CardMedia, CardContent, Typography, Grid} from "@material-ui/core";
import {useHistory} from "react-router-dom";


const defaultWriterStyles = {
    cardWidth: 6,
    cardVerticalPadding: '10vh',
    cardTopMargin: '10vh',
    imageWidth: '10vw',
    imageHeight: '10vw',
    nameSize: '3.75rem',

};

const smallWriterStyles = {
    cardWidth: 12,
    cardVerticalPadding: '0vh',
    cardTopMargin: '5vh',
    imageWidth: '25vw',
    imageHeight: '25vw',
    nameSize: '2.75rem',


};

export function Writer ({height, width, name, image, route}) {

    const [styleParameters, setStyleParameters] = useState(defaultWriterStyles)

    let history = useHistory();

    useEffect ( () => {
        if (width < minScreenWidths['medium']) {
            setStyleParameters(smallWriterStyles);
        } else {
            setStyleParameters(defaultWriterStyles);
        }
    }, [height, width]);

    let card_styles = {
        width: '90%', 
        marginLeft: '5%',
        marginTop: styleParameters['cardTopMargin'],
        marginRight: '5%',
        border: '1px solid lightblue',
        borderRadius: '20px'
    };

    return (
        <Grid item xs={styleParameters['cardWidth']}>
            <Card
                style = {card_styles}
                onClick = {() => history.push(route)}
            >
                <CardActionArea style={{paddingTop: styleParameters['cardVerticalPadding'],
                                        paddingBottom: styleParameters['cardVerticalPadding']
                }}>
                    <Grid container>
                        <Grid item xs={8}>
                            <Typography variant='h2' component='h2' 
                                    style={{fontSize: styleParameters['nameSize'],textAlign:'center'}}> 
                                {` Poems by `}
                                 <br/>
                                {`${name}`}    
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Avatar style={{'height': styleParameters['imageHeight'],
                                            'width': styleParameters['imageWidth'],}} 
                                    src={image}
                            />
                        </Grid>
                    </Grid>
                    
                
                </CardActionArea>
            
            </Card> 
        
        </Grid>
    )
} 
