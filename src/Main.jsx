import React, {useState, useEffect} from "react";
import {BrowserRouter as Router, Switch, Link, Route, useLocation } from "react-router-dom";
import useWindowDimensions from "./customHooks/UseWindowDimensions";
import {HomePage, NotFoundPage, PoemsByParman, PoemsByKhushi, ContactUs} from "./Pages";
import Header from "./Header";
import {Navbar} from "./Navbar";
import 'fontsource-roboto';

const pagesToPaths = {
    'home': <HomePage />,
    'parman_poems': <PoemsByParman />,
    'khushi_poems': <PoemsByKhushi />,
    'contact': <ContactUs/>,
    'not_found': <NotFoundPage/>,
}

const pathIndex = {
    '/parman_poems': 0,
    '/khushi_poems': 1,
    '/contact': 2
  };
  

function Pages () {
    const location = useLocation();

    const {height, width} = useWindowDimensions();
    
    const [currentLocation, setCurrentLocation] = useState(pathIndex[location.pathname]);

    useEffect ( () => {
        if (currentLocation !== pathIndex[location.pathname]) {
            setCurrentLocation(pathIndex[location.pathname])
        }
    }, [location.pathname, currentLocation])

    const renderPage = (pageToRender) => {
        if (pageToRender === 'home') {
            return (
                <>
                    <Header height={height} width={width} />
                    <HomePage/>
                </>
            )
        } else {
            return (
                <>
                    <Header height={height} width={width} />
                    <Navbar pathIndex={currentLocation} height={height} width={width} />
                    {pagesToPaths[pageToRender]}
                </>
            )
        }
    }   

    return (
        <>
            <Switch>

                <Route exact path='/'> {renderPage('home')} </Route>

                <Route path='/parman_poems'> {renderPage('parman_poems')} </Route>

                <Route path='/khushi_poems'>{renderPage('khushi_poems')} </Route>

                <Route path='/contact'> {renderPage('khushi_poems')} </Route>

                <Route> {renderPage('not_found')} </Route>

            </Switch>
        </> 
    )
}

export default function Main () {
    return (
        <Router>
            <Pages />
        </Router>
    )
}