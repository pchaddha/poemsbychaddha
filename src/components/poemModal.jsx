import React, {useEffect, useRef} from "react";

import Dialog from "@material-ui/core/Dialog";
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export function PoemModal({poemParams, isModalOpen, closeModal}) {
    const descriptionElementRef = useRef(null);

    // If the modal is open, focus the description element!
    useEffect(() => {
        if (isModalOpen) {
          const { current: descriptionElement } = descriptionElementRef;
          if (descriptionElement !== null) {
            descriptionElement.focus();
          }
        }
      }, [isModalOpen]);

    return (
        <Dialog
            open={isModalOpen}
            onClose={closeModal}
            scroll="paper"
            aria-labelledby="modal-for-poem"
            aria-describedby={`modal-for-poem-name-${poemParams.name}`}
        >
            <DialogTitle id="scroll-dialog-title"> 
                {poemParams.name}
            </DialogTitle>

            <DialogContent dividers={true}>
                <DialogContentText
                    id="scroll-dialog-description"
                    ref={descriptionElementRef}
                    tabIndex={-1}
                >
                    <p style={{whiteSpace: "pre-line"}}> {poemParams.body} </p>                    
                </DialogContentText>
            </DialogContent>

            <DialogActions>
                <Button onClick={closeModal}>Cancel</Button>
            </DialogActions>
        </Dialog>
    )
}