import React, {useEffect, useState} from "react";
import {PoemsByParmanObject} from "./PoemsByParmanObject";
import { makeStyles } from '@material-ui/core/styles';
import {Card, CardActionArea, CardActions, CardContent, CardMedia, Button, Typography, Chip, IconButton} from "@material-ui/core";
import {ShareOutlined, FavoriteBorder} from "@material-ui/icons";
import useWindowDimensions from "../customHooks/UseWindowDimensions";
import {minScreenWidths} from "../ConfigurationParameters";
import {PoemModal} from "../components/poemModal";

function PoemCard ({name, image, body, tags, poemClicked}) {
    let {height, width} = useWindowDimensions();
    let image_width;
    let margin_width;
    if (width < minScreenWidths['medium']) {
        image_width = '95vw'
        margin_width = '2.5vw'
    } else {
        image_width = '23vw'
        margin_width = '0.5vw'
    }
    return (
        <Card style={{borderRadius: 25, display: 'inline-block', width: image_width, marginLeft: margin_width, marginRight: margin_width}}>
            <CardActionArea onClick={() => poemClicked(name)}>
                <CardMedia
                    component="img"
                    alt={name}
                    height='100'
                    image={image}
                    title={name}
                />

                <CardContent>
                    <Typography style={{textAlign:'center'}} gutterBottom variant="h5" component="h2">
                        {name}
                    </Typography>

                    <TagChips tags={tags} />                    
                </CardContent>

            </CardActionArea>

            <CardActions>
                <IconButton size="small" color="primary"> <FavoriteBorder/></IconButton>
                <IconButton size="small" color="primary">  <ShareOutlined/> </IconButton>
            </CardActions>            
        </Card>
    )
};

function TagChips({tags}) {
    return (
        <Typography style={{float: 'left'}}>
            {tags.map( (tag, value) => {
                return (
                    <Chip 
                        style={{marginRight:'0.25vw'}}
                        label={tag}
                        key={value}
                        color="primary"
                        variant="outlined"
                    />
                )
            })}
        </Typography>
    )
};


export default function PoemsByParmContent () {
    const [activePoem, setActivePoem] = useState();
    const [isModalOpen, setIsModalOpen] = useState(false);
    function openPoem(name) {
        setActivePoem(name);
        setIsModalOpen(true);
    };

    function closeModal() {
        setIsModalOpen(false);
        console.log("modal closed");
    };

    function getPoem() {
        return PoemsByParmanObject.filter((row) => row.name === activePoem)[0];
    };

    return (
        <>
            {PoemsByParmanObject.map((row, index) => {
                return (
                    <>
                        <PoemCard 
                            key={`parmanpoem${index}`}
                            poemClicked={() => openPoem(row.name)}
                            {...row}
                        />
                </>

                )
            })}

            {activePoem && <PoemModal
                poemParams={getPoem(activePoem)}
                isModalOpen={isModalOpen}
                closeModal={() => closeModal()}
            />}
        </>
    );
}