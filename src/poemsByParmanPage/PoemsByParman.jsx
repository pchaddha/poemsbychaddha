import { Typography } from "@material-ui/core";
import React, {useState, useEffect} from "react";
import {minScreenWidths} from "../ConfigurationParameters";
import useWindowDimensions from "../customHooks/UseWindowDimensions";
import PoemsByParmanContent from "./PoemsByParmanContent";

const defaultStyleParameters = {
    pictureWidth: '5.0vw',
    pictureHeight: '5vw',
    titleSpace: 12,
    imageSpace: 1,
    titleVariant: 'h2'
}

const smallScreenParameters = {
    pictureWidth: '40vw',
    pictureHeight: '20vw',
    titleSpace: 12,
    imageSpace: 6,
    titleVariant: 'h4'
}

const bothScreenParameters = {
    title: {
        textAlign: 'center',
    }
}

const stylesToUse = (width) => {
    if (width < minScreenWidths['medium']) {
        return smallScreenParameters
    } else {
        return defaultStyleParameters
    }
}

function PageTitle ({bothScreenParameters, styleParameters, name}) {
    return (
        <Typography style={bothScreenParameters.title} variant={styleParameters.titleVariant} component="h1">
            {name}
        </Typography>
    )
}

export default function PoemsByParmanPage (props) {
    
    let {height, width} = useWindowDimensions();

    const [styleParameters, setStyleParameters] = useState(stylesToUse(width));

    return (
        <>
        <PoemsByParmanContent/>
        </>
    )
}
