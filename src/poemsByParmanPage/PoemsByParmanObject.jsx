import {busPoem} from "../poems/bus";
import {EPPoem} from "../poems/EP";
import {menOfClothPoem} from "../poems/menofcloth";
import {theSpeechPoem} from "../poems/thespeech";
import {californiaPoem} from "../poems/california";
import {americaTheGreatPoem} from "../poems/americathegreat";
import { FriendsPoem } from "../poems/friends";
import { osPoem } from "../poems/os";
import { theWindowPoem } from "../poems/theWindow";
import {thePerfectRelationshipPoem} from "../poems/thePerfectRelationship";
import {APoem} from "../poems/A";
import { RPoem } from "../poems/R";

export const PoemsByParmanObject = [

    {...EPPoem},
    
    {...menOfClothPoem},
    
    {...FriendsPoem},
    
    {...californiaPoem},
    
    {...busPoem},

    {...americaTheGreatPoem},

    {...theSpeechPoem},

    {...osPoem},

    {...theWindowPoem},

    {...thePerfectRelationshipPoem},

    {...APoem},

    {...RPoem},
];
