import React from "react";
import Header from "./Header";
import {MainNavbar} from "./Navbar";
import useWindowDimensions from "./customHooks/UseWindowDimensions";
import NotFound from "./notFoundPage/NotFoundPage";
import MainPage from "./mainPage/MainPage";
import PoemsByParmanPage from './poemsByParmanPage/PoemsByParman';

export function HomePage () {
    const {height, width} = useWindowDimensions();
    return (
        <>
        <MainNavbar height={height} width={width}/>
        </>
    )
}

export function PoemsByParman(props) {
    return (
        <PoemsByParmanPage />
    )
}


export function PoemsByKhushi() {

    return (
        <>
        </>
    )
}


export function ContactUs() {

    return (
        <>
        </>
    )
}



export function NotFoundPage () {
    return (
        <NotFound/>
    )
}

